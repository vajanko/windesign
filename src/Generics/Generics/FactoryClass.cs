﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics
{
    static class FactoryClass
    {
        public static string ConfigFile = "C:\\tmp\\config.xml";

        public static TService CreateService<TService>() where TService : IInitializable, new()
        {
            TService service = new TService();
            service.Initialize(ConfigFile);

            return service;
        }
    }

    interface IInitializable
    {
        void Initialize(string configFile);
    }
    class DatabaseService
    {
        public DatabaseService()
        {

        }
    }
    class FileService
    {
        public FileService()
        {

        }
    }
    class NetworkService
    {

    }
}
