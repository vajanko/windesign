﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics
{
    static class GenericMethod
    {
        public static void DoForEveryJohn<T>(List<T> collection) where T : IDispalyName
        {
            for (int i = 0; i < collection.Count; i++)
            {
                T current = collection[i];
                if (current.DisplayName == "John")
                {
                    current.DisplayName = "Johnny";
                }
            }
        }

        public static void Test1()
        {
            List<Customer> customers = new List<Customer>();
            List<Employee> employees = new List<Employee>();

            DoForEveryJohn<Customer>(customers);
            DoForEveryJohn(customers);

            //DoForEveryJohn(employees);
            //DoForEveryJohn(employees, RenameJohn);
            //DoForEveryJohn(employees, e => { if (e.UserName == "John") e.UserName = "Johnny"; });
        }
        public static void RenameJohn(Employee emp)
        {
            if (emp.UserName == "John")
            {
                emp.UserName = "Johnny";
            }
        }

        public static void DoForEveryJohn<T>(List<T> collection, Action<T> action)
        {
            for (int i = 0; i < collection.Count; i++)
            {
                if (i % 2 == 0)
                {
                    T current = collection[i];
                    action(current);
                }
            }
        }
    }

    interface IDispalyName
    {
        string DisplayName { get; set; }
    }

    class Employee
    {
        public string UserName { get; set; }
    }
    class Customer : IDispalyName
    {
        public string DisplayName { get; set; }
    }
}
