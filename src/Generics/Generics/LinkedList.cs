﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

namespace Generics
{
    class Node
    {
        public int Data { get; set; }
        public Node Next { get; set; }

        public Node(int value)
        {
            Next = null;
            Data = value;
        }
    }

    class MyList
    {
        private Node head;

        public MyList()
        {
            head = null;
        }

        public void AddHead(int value)
        {
            Node n = new Node(value);
            n.Next = head;
            head = n;
        }

        public IEnumerator GetEnumerator()
        {
            Node current = head;

            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }

        //public int MaxValue()
        //{
        //    int max = 0;
        //    foreach (int val in this)
        //        if (val > max)
        //            max = val;

        //    return max;
        //}
        //public int CountOccurences(int value)
        //{
        //    int count = 0;
        //    foreach (int val in this)
        //        if (val == value)
        //            count++;

        //    return count;
        //}
    }

    public static class MyListTest
    {
        public static void Test()
        {
            MyList list = new MyList();
            list.AddHead(10);
            list.AddHead(20);
            list.AddHead(30);

            foreach (int value in list)
            {
                Console.WriteLine(value);
            }
        }
    }
}
