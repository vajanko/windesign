﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListBinding
{
    class Database
    {
        private string dataFile;

        public Database()
        {
            dataFile = ".\\data.dat";
        }
        public Database(string dataFile)
        {
            this.dataFile = dataFile;
        }

        public List<Person> LoadData()
        {
            if (!File.Exists(dataFile))
                return new List<Person>();

            string str = File.ReadAllText(dataFile);
            List<Person> data = JsonConvert.DeserializeObject<List<Person>>(str);
            return data;
        }
        public void SaveData(List<Person> data)
        {
            string str = JsonConvert.SerializeObject(data);
            File.WriteAllText(dataFile, str);
        }
    }
}
