﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListBinding
{
    public class Person
    {
        private string firstName;
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        public string LastName { get; set; }

        public int Age { get; set; }

        public bool IsIndividual { get; set; }

        private string name;
        public string Name
        {
            get
            {
                if (IsIndividual)
                    return FirstName + " " + LastName;
                else
                    return name;
            }
            set
            {
                if (!IsIndividual)
                    name = value;
            }
        }

        public Gender Gender { get; set; }

        //public DateTime Birthday { get; set; }
    }

    public enum Gender
    {
        Male,
        Female
    }

    public class GenderItem
    {
        public string Name { get; set; }
        public Gender Value { get; set; }
    }

}
