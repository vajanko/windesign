﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListBinding
{
    public partial class MainForm : Form
    {
        private Database database = new Database(".\\mydata.dat");

        public MainForm()
        {
            InitializeComponent();

            genderSource.DataSource = new List<GenderItem>()
            {
                new GenderItem { Name = "Male", Value = Gender.Male },
                new GenderItem { Name = "Female", Value = Gender.Female },
            };
        }
    }
}
